
const DEV = process.env.NODE_ENV;

export function logErrorCatch(description: string, err: [] | object | string | number | null) {
    console.log(description, err);
}
  
export function logDev(description = "", data = []) {
    if (DEV == 'development') {
        console.log(description, data);
    }
}