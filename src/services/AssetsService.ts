import { Base64Req, IRespGetInvoice } from "@/models/types";
import { axiosInstance } from "@/services/http";
import { AxiosResponse, AxiosRequestConfig } from "axios";

export const saveBase64 = async (data: Base64Req) => {
  const urlFull = `https://data.aveonline.co/api/v1/p2p/create`;
  try {
    const dataResponse: AxiosResponse = await axiosInstance.post(urlFull, data);
    if (dataResponse) {
      const resposeAve = dataResponse.data;
      return Promise.resolve(resposeAve);
    }
  } catch (err) {
    return Promise.reject(err);
  }
};

export const getInvoice = async (invoice: string) => {
  const urlFull = `https://data.aveonline.co/api/v1/invoices/show/${invoice}`;
  // const urlFull = `https://aveonline.co/api/comunes/v2.0/facturacion.php`;
  try { 
    const dataResponse: AxiosResponse = await axiosInstance.get(urlFull);
    if (dataResponse) {
      const resposeAve:IRespGetInvoice = dataResponse.data;
      return Promise.resolve(resposeAve);
    }
  } catch (err) {
    return Promise.reject(err);
  }
};

////////////////////////////////////////////////
export const getBlobDownlader = async (uri: string) => {
  const uriBase = 'https://aveonline.co/assets/';
  const url = `${uriBase}${uri}`;
     
  const config:AxiosRequestConfig = { 
    responseType: 'blob', 
    baseURL: uriBase,
    withCredentials: false
  };
  axiosInstance.head(url, config)
  try {
    // const fileName = 'file.pdf'
    // const resp = await axiosInstance.get(url);
    // const Blob = resp.data.blod();
    // const objectURL = URL.createObjectURL(Blob);
    // const file = new File([Blob], fileName, { type: Blob.type })
  } catch (err) {
    return Promise.reject(err);
  }
}