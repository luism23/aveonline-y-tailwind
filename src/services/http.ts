import axios from "axios";
import { filterTransporters, getLocalToken } from '@/utils/';
import { IDatosRemintenteResp, IGetRefWompyReq, IGetResultPayWompi, IPostNotificarPago, IRespGeneAve, IRespGeneAveData, IRespGeneAveTransp, IResultPayWompiResp } from "@/models/types";

const auth_token = getLocalToken(); 

export const modoDEV = process.env.NODE_ENV === "development" ? true: false;   
export  const baseURL = process.env.NODE_ENV === "development"
  ? "http://localhost"
  : "https://aveonline.co";

export const axiosInstance = axios.create({
    baseURL,
    headers: {
        Authorization: `${auth_token}`
    }
});


/**
 * Solo para usar en DEV
 * @returns 
 */
export const GetToken = async () => {
    console.log('www');
    
}

export const GetDatosRemitente =  async (idExp: number) => {
    const urlFull = `https://aveonline.co/api/courier/dhl/v1.0/datosremitente.php`;
    try {
        const body = {
            tipo: "listar",
            idempresa: idExp
        }
        const dataResponse = await axiosInstance.post(urlFull, body);
        if (dataResponse) {
            const resposeAve:IDatosRemintenteResp =  dataResponse.data
            return Promise.resolve(resposeAve.cliente);
        }
    } catch (error) {
        console.log(error);
        return Promise.reject(error)
    }
}

export const PostNotificacionPago =  async (body:IPostNotificarPago ) => {
    const urlFull = `https://aveonline.co/api/comunes/v2.0/pagosListado.php`;
    try {
        const dataResponse = await axiosInstance.post(urlFull, body);
        if (dataResponse) {
            const resposeAve:IRespGeneAve =  dataResponse.data
            return Promise.resolve(resposeAve);
        }
    } catch (error) {
        console.log(error);
        return Promise.reject(error)
    }
}

export const PostListarTransProveedor =  async () => {
    const urlFull = `https://aveonline.co/api/box/v1.0/transportadora.php`;
    try {
        const body = {
            tipo: "listaSimple"
        }
        const dataResponse = await axiosInstance.post(urlFull, body);
        if (dataResponse) {
            const resposeAve:IRespGeneAveTransp =  dataResponse.data
            const result = filterTransporters(resposeAve.transportadoras);
            console.log('resul',result);
            
            return Promise.resolve(resposeAve);
        }
    } catch (error) {
        console.log(error);
        return Promise.reject(error);
    }
}

export const PayToWompy =  async (body:IGetRefWompyReq ) => {
    const urlFull = `https://aveonline.co/api/comunes/v2.0/pagosWompi.php`;
    try {
        const dataResponse = await axiosInstance.post(urlFull, body);
        if (dataResponse) {
            const resposeAve:IRespGeneAveData =  dataResponse.data
            return Promise.resolve(resposeAve);
        }
    } catch (error) {
        console.log(error);
        return Promise.reject(error)
    }
}

export const validatePayWompi =  async (id: string) => {
    const urlFull = `https://aveonline.co/api/wompi/index.php`;
    try {
        const body: IGetResultPayWompi = {
            tipo: "getResposeId",
            id_wompi: id,
            env_test: true
        }
        const dataResponse = await axiosInstance.post(urlFull, body);
        if (dataResponse) {
            const resposeAve:IResultPayWompiResp =  dataResponse.data
            return Promise.resolve(resposeAve);
        }
    } catch (error) {
        console.log(error);
        return Promise.reject(error)
    }
}






