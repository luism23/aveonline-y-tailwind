import { axiosInstance } from '@/services/http';
import { IResponseAveRecaudos } from "@/models/types/ResponseAve";
import { AxiosResponse } from "axios";
import { ICostosAproxResp, IFacturasReq, IFacturasResp, INotasRecibosResp, IRecaudosReq } from '../models/types/index';

export const GetRecuados = async (data: IRecaudosReq ) => {
    // const urlFull = `${baseURL}/api/recaudos/v1.0/index.php`;
    const urlFull = `https://aveonline.co/api/recaudos/v1.0/index.php`;    
    try {   
        const dataResponse:AxiosResponse = await axiosInstance.post(urlFull, data);
        if (dataResponse) {
            const resposeAve: IResponseAveRecaudos =  dataResponse.data
            return Promise.resolve(resposeAve.data);
        }
    } catch (err) {
        return Promise.reject(err);
    }
}

export const GetFacturas = async (data: IFacturasReq ) => {
    // const urlFull = `${baseURL}/api/recaudos/v1.0/index.php`;
    const urlFull = `https://aveonline.co/api/recaudos/v1.0/index.php`;
    try { 
        const dataResponse:AxiosResponse = await axiosInstance.post(urlFull, data);
        if (dataResponse) {
            const resposeAve:IFacturasResp =  dataResponse.data
            return Promise.resolve(resposeAve);
        }
    } catch (err) {
        return Promise.reject(err);
    }
}

export const GetRecibosYnotas = async (data: IFacturasReq ) => {
    // const urlFull = `${baseURL}/api/recaudos/v1.0/index.php`;
    const urlFull = `https://aveonline.co/api/recaudos/v1.0/index.php`;
    try { 
        const dataResponse:AxiosResponse = await axiosInstance.post(urlFull, data);
        if (dataResponse) {
            const resposeAve:INotasRecibosResp =  dataResponse.data
            return Promise.resolve(resposeAve);
        }
    } catch (err) {
        return Promise.reject(err);
    }
}

export const GetGastosAprox = async (data: IFacturasReq ) => {
    // const urlFull = `${baseURL}/api/recaudos/v1.0/index.php`;
    const urlFull = `https://aveonline.co/api/recaudos/v1.0/index.php`;
    try { 
        const dataResponse:AxiosResponse = await axiosInstance.post(urlFull, data);
        if (dataResponse) {
            const resposeAve:ICostosAproxResp =  dataResponse.data
            return Promise.resolve(resposeAve);
        }
    } catch (err) {
        return Promise.reject(err);
    }
}

/* Accesos al Service *****/
/// → src\components\recaudos\TheTitleHeader.vue