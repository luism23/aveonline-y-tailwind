
import { REGEX } from '@/constants/'

export const Validator = (values: string | number) => {
  const val = values;
  return {
    string(rules: string[] = [] , regex = /()\w+/g): boolean {
      try {
        if ( val && typeof val === 'string' ) {
          if (!val.match(regex)) { return false; }
          if (rules) {
            let errs = 0;
            rules.forEach(r => {
              const min = r.includes('min') ? true : false;
              const max = r.includes('max') ? true : false;
              if (min || max) {
                const len = Number(r.split(':')[1]);
                if(min && val.length < len) {
                  errs++;
                }
                if(max && val.length > len) {
                  errs++;
                }
              }
            }) 
            return errs > 0 ? false : true;
          }
          return true;
        }
        return false;
        
      } catch (err) {
        return false;
      }
    },
    number(rules: string[] = []): boolean  {
      try {
        const valNumber = Number(val);
        if ( val && typeof valNumber === 'number' ) {
          if (rules) {
            let errs = 0;
            rules.forEach(r => {
              const min = r.includes('min') ? true : false;
              const max = r.includes('max') ? true : false;
              if (min || max) {
                const len = Number(r.split(':')[1]);
                if(min && val < len) {
                  errs++;
                }
                if(max && val > len) {
                  errs++;
                }
              }
            }) 
            return errs > 0 ? false : true;
          }
          return true;
        }
        return false;
      } catch (err) {
        return false;
      }
    },
    email(): boolean {
      if ( val && typeof val === 'string' ) {
        const match = val.match(REGEX.EMAIL);
        return match ? true : false;
      }
      return false;
    },
    date(): boolean {
      if ( val && typeof val === 'string' ) {
        const match = val.match(REGEX.FECHAS);
        return match ? true : false;
      }
      return false;
    },
    phone(): boolean {
      if ( val && (typeof val === 'string' || typeof val === 'number' )) {
        const match = String(val).match(REGEX.TELEFONO);
        return match ? true : false;
      }
      return false;
    },
    pattern(reg: RegExp): boolean {
      if ( val && (typeof val === 'string' || typeof val === 'number' )) {
        const match = String(val).match(reg);
        return match ? true : false;
      }
      return false;
    },
    allowed(allow: number[] | string[]): boolean {
      if ( val && allow && (typeof val === 'string' || typeof val === 'number' )) {
        let ok = 0;
        allow.forEach(el => {
          if (String(val) == String(el)) {
            ok++;
          }
        })
        return ok == 1 ? true : false;
      }
      return false;
    }
  }
}