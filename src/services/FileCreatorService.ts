import { IDataFacturaCSV, IFacturasData, INotasIAData, IRecaudosData, TypeRowCSV } from "@/models/types";
import { clearHtmlEntities, clearObs } from "@/utils";
type TypeFile = "img" | "pdf"


/**
 * Verificador de nombres
 * @param name
 * @returns
 */
const createDocumentName = (name: string): string | boolean => {
  const nameSplit = name.split(".");
  if (nameSplit[2]) {
    return false;
  } else if (nameSplit[1]) {
    if (nameSplit[1] != "csv") {
      return false;
    }
    return `${nameSplit[0]}.${nameSplit[1]}`;
  } else {
    return name;
  }
};

/**
 * Crea un archivo CSV, apartir de un array de json
 * @param arrayOfJson Array de objetos con lo que va llenar el documento;
 * @param headersPrint "OPCIONAL" Array nombres de las columnas, sino se manda se crea el documento con los key del objeto
 * @param nameDoc "OPCIONAL" nombre del documento, sino se crea como "download.csv"
 */
export const DownLoadCSV = (
  arrayOfJson: object[],
  headersPrint: string[] = [],
  nameDoc = ""
): void => {
  let Name: string | boolean = "";
  if (nameDoc != "") {
    Name = createDocumentName(nameDoc);
    if (Name == false) {
      throw new Error(
        "nombre no permito, no se acepta más de un punto en el nombre"
      );
    }
  }

  const filename = nameDoc ? `${Name}` : "download.csv";
  const replacer = (key: string, value: string | number) =>
    value === null ? "" : value;
  const header = Object.keys(arrayOfJson[0]);

  const csv = arrayOfJson.map((row: TypeRowCSV) => {
    return header
      .map((fieldName: string | number) => {
        return JSON.stringify(row[fieldName], replacer);
      })
      .join(";");
  });
  const headerP = headersPrint ? headersPrint : header;
  csv.unshift(headerP.join(";"));
  const csvFormated = csv.join("\r\n");

  const link = document.createElement("a");
  link.setAttribute(
    "href",
    "data:text/csv;charset=utf-8,%EF%BB%BF" + encodeURIComponent(csvFormated)
  );
  link.setAttribute("download", filename);
  link.style.visibility = "hidden";
  document.body.appendChild(link);
  link.click();
  document.body.removeChild(link);
};

/**
 * Funcion Auxiliar de csvConvertJSON()
 */
const converLinesToJson = (
  arrayLines: string[][],
  keys: string[]
): object[] => {
  let header: string[] = keys.length > 0 ? keys : [];
  const arrayObj: object[] = [];
  arrayLines.forEach((line, k) => {
    const obj: { [k: string]: string } = {};
    if (k == 0) {
      header = line.map((l) => l.replace(/ +/g, "_").toLowerCase());
    } else {
      line.forEach((l, k) => {
        const keyObject: string = header[k];
        obj[`${keyObject}`] = l;
      });
    }
    arrayObj.push(obj);
  });
  const ArrayObjects = arrayObj.filter((obj) => {
    const keys = Object.keys(obj);
    if (keys.length > 0) {
      return obj;
    }
  });

  return ArrayObjects;
};
/**
 * Convierte un archivo CSV a json
 */
export const csvConvertJSON = (fileInput: Event, keys: string[] = []) => {
  return new Promise((resolver, reject) => {
    try {
      const target = fileInput.currentTarget as HTMLInputElement;

      if (target && target.files) {
        const fileReaded = target.files[0];

        const reader: FileReader = new FileReader();
        reader.readAsText(fileReaded);

        reader.onload = () => {
          if (typeof reader.result == "string") {
            const csv: string = reader.result;
            const allTextLines = csv.split(/\r|\n|\r/);
            const headers = allTextLines[0].split(",");
            const lines:string[][] = [];

            for (let i = 0; i < allTextLines.length; i++) {
              const data = allTextLines[i].split(",");
              if (data.length === headers.length) {
                const tarr:string[] = [];
                for (let j = 0; j < headers.length; j++) {
                  tarr.push(data[j]);
                }
                if (tarr[0] !== "") {
                  const data = tarr[0].split(";");
                  lines.push(data);
                }
              }
            }
            resolver(converLinesToJson(lines, keys));
          }
        };
      }
    } catch (error) {
      console.log(error);
      reject(error);
    }
  });
};

/**
 * Descarga el base64 en un archivo ya construido
 * @param base / base64 completa con el mime type
 * @param name / nombre del archivo
 */
export const dowmloadFile = (base: string, name: string):void => {
  const link = document.createElement("a");
  link.setAttribute("href", base);
  link.setAttribute("download", name);
  link.style.visibility = "hidden";
  document.body.appendChild(link);
  link.click();
  document.body.removeChild(link);
}

/**
 * Decide si el base64 q recibe lo muestra en una etiqueta Iframe o Img
 * @param base / base64 completa con el mime type
 * @param name / nombre la ventana
 * @param type / si es tipo imagen o pdf
 */
export const showOpenFile = (base: string, name: string, type: TypeFile ): void => {
  const baseSplit = base.split(',')
  baseSplit[1] = encodeURI(baseSplit[1]);
  if (type == 'pdf') {
    window.open("", name, "width=600,height=800")?.document.write(
      "<iframe width='100%' height='100%' src='data:application/pdf;base64, " +
      baseSplit[1] + "'></iframe>"
    )
  } else {
    window.open("", name, "width=600,height=800")?.document.write(
      "<img width='100%' height='100%' href='data:application/pdf;base64, " +
      baseSplit[1] + "'></img>")
  }
}

/**
 * Convierte los datos de las respuesta de facturas a datos para el csv
 */
export const prepareInvoiceToCSV = (array:IFacturasData[]) => {
  const dataArray: IDataFacturaCSV[] = []
  array.forEach((fact) => {
    dataArray.push({
      castigada: '',
      cliente: fact.dsrazon,
      nit: `${fact.dsnit}`,
      observaciones: clearObs(fact.dsobs),
      ven: fact.dias_venc ? 'SI' : 'NO',
      factura: `${fact.dsprefijo} ${fact.idpedido}`,
      fecha_factura: fact.dsfechac,
      fecha_venc: fact.dsfechav,
      dias: 0,
      valor: fact.dstotal,
      fecha_abon1: fact.recibos[0] ? fact.recibos[0].dsfecha : '',
      valor_abon1: fact.recibos[0] ? fact.recibos[0].dsvalor : null,
      fecha_abon2: fact.recibos[1] ? fact.recibos[1].dsfecha : '',
      valor_abon2: fact.recibos[1] ? fact.recibos[1].dsvalor : null,
      fecha_abon3: fact.recibos[2] ? fact.recibos[2].dsfecha : '',
      valor_abon3: fact.recibos[2] ? fact.recibos[2].dsvalor : null,
      fecha_abon4: fact.recibos[3] ? fact.recibos[3].dsfecha : '',
      valor_abon4: fact.recibos[3] ? fact.recibos[3].dsvalor : null,
      valor_nc: fact.notas_d_credito[0] ? fact.notas_d_credito[0].dsvalor : null,
      saldo: fact.saldo
    })
  });
  return dataArray;
}


export const prepareCompensationsToCSV = (data: INotasIAData[]) => {
  return data.map(el => {
    return {
      factura: el.dsfactura,
      fecha: el.dsfecha,
      descripcion: clearHtmlEntities(el.descripcion),
      reclamacion: el.dsnumero,
      valor: el.dsvalor,
      egresos: el.dsnaturaleza == 2 ? el.dsvalor * (-1) : el.dsvalor,
      billera: el.dsnaturaleza == 2 ? 0 : el.dsvalor
    }
  })
}

export const prepareGuidesToCSV = (data: IRecaudosData[]) => {
  return data.map(el => {
    return {
      numero_guía: el.numero_guia,
      fecha: el.dsfecha,
      contenido: el.dscom,
      valor_recaudo: el.dsvalorrecaudo,
      valor_cliente: el.dstotalave_fc,
      billera: el.dsvalorrecaudo - el.dstotalave_fc
    }
  })
}

export const prepareCreditNotesToCSV = (data: INotasIAData[]) => {
  return data.map(el => {
    return {
      factura: el.dsfactura,
      fecha: el.dsfecha,
      descripcion: clearHtmlEntities(el.descripcion),
      reclamacion: el.dsnumero,
      valor: el.dsvalor,
      egresos: el.dsnaturaleza == 2 ? el.dsvalor * (-1) : el.dsvalor,
      billera: el.dsnaturaleza == 2 ? 0 : el.dsvalor
    }
  })

}
