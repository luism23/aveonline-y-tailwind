import axios from "axios";
import { IResp_SendMsg, ISendMsg, ISaveAccountBank, 
         IResqGetlisBank, ILisBankResp, IRespListSusbcripts, IBankSendPays, IRespGeneAve, IFavoriteAccount } from "@/models/types";
import { coverUpIdUser, coverUpIdEnterprise } from "@/utils";

const baseUrlH2H = 'https://api.aveonline.co/api-h2h/public/api/';

export const axiosInstance = axios.create({
    baseURL: baseUrlH2H
});

/**
 * Envia los datos de la cuenta para ya ser guardada, ya tiene el código de confirmacion
 * @param body 
 * @returns 
 */
export const PostNewAccountBank =async (body: ISaveAccountBank) => {
    const url = `v1/wallet/inscriptionAcount`;
    try {
        const dataResponse = await axiosInstance.post(url, body);
        if (dataResponse) {
            const resposeAve =  dataResponse.data
            return Promise.resolve(resposeAve);
        }
    } catch (err) {
        return Promise.reject(err);
    }
}
/**
 * Trae la lista de los bancos a medida q se van escribiendo en el buscador
 * @param body 
 * @returns 
 */
export const GetListBanks = async (body:IResqGetlisBank): Promise<ILisBankResp[] | undefined> => {
    const url = `v1/h2h/getBanks`;
    try {
        const dataResponse = await axiosInstance.post(url, body);
        if (dataResponse) {
            const resposeAve =  dataResponse.data
            return Promise.resolve<ILisBankResp[]>(resposeAve);
        }
    } catch (err) {
        return Promise.reject(err);
    }
}
/**
 * Trae la lista de las cuentas subscriptas del cliente
 * @param body 
 * @returns 
 */
export const GetSubscribedAccounts = async (body:ISendMsg): Promise<IRespListSusbcripts[] | undefined> => {
    const url = `v1/wallet/getListAccount`;
    try {
        const dataResponse = await axiosInstance.post(url, body);
        if (dataResponse) {
            const resposeAve =  dataResponse.data            
            return Promise.resolve<IRespListSusbcripts[]>(resposeAve.data);
        }
    } catch (err) {
        return Promise.reject(err);
    }
}

/**
 * Post para hacer el retiro del dinero
 * @param body 
 * @returns 
 */
export const PostDisbursementClient = async (body:IBankSendPays): Promise<IRespGeneAve | undefined> => {
    const url = `/v1/wallet/insertPayment`;
    try {
        const dataResponse = await axiosInstance.post(url, body);
        if (dataResponse) {
            const resposeAve =  dataResponse.data
            return Promise.resolve<IRespGeneAve>(resposeAve);
        }
    } catch (err) {        
        return Promise.reject(err);
    }
}
/**
 * Put para el cambio de cuenta favorita
 * @param body 
 * @returns 
 */
export const ChangeFavoriteAccount = async (body:IFavoriteAccount): Promise<IRespListSusbcripts[] | undefined>=> {
    const url = `/v1/wallet/acountFavorite`;
    try {
        const dataResponse = await axiosInstance.put(url, body);
        if (dataResponse) {
            if (dataResponse) {
                const resposeAve =  dataResponse.data            
                return Promise.resolve<IRespListSusbcripts[]>(resposeAve.data);
            }
        }
    } catch (err) {        
        return Promise.reject(err);
    }
}

/**
 * Solicitud del mensaje de texto para la confirmacion del desembolso de dinero
 * @returns Promises
 */
 export const RequestMessagePayDisbursement = async (idUser: string, idEnterprise: string) => {
    const url = `v1/h2h/requestSMSH2H`;
    const body:ISendMsg = {
        userEnterprise: coverUpIdUser(idUser),
        enterprise: coverUpIdEnterprise(idEnterprise)
    }
    try {
        const dataResponse = await axiosInstance.post<Promise<IResp_SendMsg>>(url, body);
        if (dataResponse) {
            const { data } = dataResponse;
            return Promise.resolve(data);
        }
    } catch (err) {
        return Promise.reject(err);  
    }
}




