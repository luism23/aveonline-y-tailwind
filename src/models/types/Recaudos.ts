export interface IRecaudosReq { // data fake
	tipo : string
	data : {
		idtransportador: string,
		dsconsec: string,
		dsvalorrecaudo: string,
		dsfechai: string,
		dsfechaf: string,
		dsciudad: string,
		dsciudadd: string,
		idexp: string,
		idcampobase: string,
		dsempresap: string
	}
} 
type typeFacturasReq = "facturas" | "notasRecibos" |"costosAprox";
export interface IFacturasReq {
	tipo : typeFacturasReq, 
	data: {
		dsfechai: string,
		dsfechaf: string,
		idexp: string
	}
}

export interface IResponseAllDataRecaudos {
	arecaudar: IRecaudosData[];
	devolucion: IRecaudosData[];
	indemnizado: IRecaudosData[];
	liquidadas: IRecaudosData[];
	proceso_indemnizacion: IRecaudosData[];
	recaudadas: IRecaudosData[];
	totales: IRecaudosTotales
}

export interface IRecaudosTotales {
	arecaudar: number;
	devoluciones: number;
	indemnizaciones: number;
	liquidadas: number;
	liquidadas_billetera: number;
	procesoindemnizacion: number;
	recaudadas: number;
	sinmovimiento: number;
}

export interface IRecaudosData {
	
	numero_guia: string;
	idtransportador: string;
	dsconsec: string;
	pkid: string;
	idagente: string;
	agente: string;
	codagente: string;
	idcliente: string;
	dscomtotal: string;
	dscom: string;
	tipodoc: string;
	numdoc: string;
	dsfecha: string;
	idexp: string;
	dsres: string;
	dsprefijofactura: string;
	dscontrol: string;
	dsnumfacturaave: string;
	dsnumfacturaave_fc: string;
	dsres_fc: string;
	recibo: number;
	egreso: number;
	valoregreso: number;
	fechaegreso: number;
	estadorecaudotxt: string;
	valortransrecaudo: number;
	flete: number;
	dstotal: number;
	dscostomanejo: number;
	idsercontraentrega: string;
	dsvalorrecaudo: number;
	idasumecosto: string;
	dscostorecaudo: number;
	dsnombred: string;
	dsestado: string;
	empresa: string;
	dsciudad: string;
	dsciudadd: string;
	transportadora: string;
	dscontraentrega: string;
	dsestadobox: string;
	dsfechaentrega: string;
	estadorecaudo: string;

	idfactura_fc: number;
	dsfleteave_fc: number;
	dsmanejoave_fc: number;
	dstotalave_fc: number;
	dsotrosave_fc: number;
}

export interface IRecibosArr {
	dsvalor: number,
	dsfecha: string,
	descripcion: string,
	dsfactura: string,
	dsnumero: string,
	tiporecibo: number
}
export interface IFacturasData {
	cufe: string | null;
	comisiones: number,
	compra_asistida: string | null,
	cuantiasfac: string,
	dias_venc: number | null,
	dsbase: number,
	dsbox: number,
	dscausaltexto: string | null,
	dsciudad: string,
	dsclientedsdiasv: number,
	dscontacto: string,
	dscontrol: string,
	dsct: string | null
	dsdescfinanciero: string |null,
	dsdescomercial:string | null
	dsdesct: string,
	dsdir: string,
	dsencares: string,
	dsfechac: string,
	dsfechad: string,
	dsfechap: string | null
	dsfechav: string,
	dsiva: string,
	dslogin: string,
	dsnit: number,
	dsnumfactura: string | null,
	dsobs: string,
	dsobsbox: string | null,
	dsobsfinal: string | null,
	dsobspago: string | null,
	dsobsplazo: string | null,
	dsorden: string ,
	dspedido: string ,
	dspordesct: string,
	dsprefijo: string,
	dsrazon: string,
	dsres: string,
	dsrete: string,
	dsreteica: string,
	dsreteiva: string,
	dssubtotal: number
	dstele: string,
	dstotal: number
	dstrm: string,
	dsvalor: number | null
	dsvendedor: string,
	envio_correo: number,
	fecha_envio_correo: string,
	from_Currency: string,
	idactivo: number,
	idactivost: string | null,
	idbodegaje: string | null,
	idcentrocc: number,
	idcliente: number,
	idconsolidacion: number | null,
	idfechac: number,
	idfechad: number | null,
	idfechap: number | null,
	idfechav: number,
	idformapago: number,
	idorden: number,
	idpedido: number,
	idpersonafact: number,
	idplazo: number,
	idusuario: number,
	idusuariocreador: number,
	inspeccion: string | null,
	manejo_box: string | null,
	pordsdescfinanciero: string | null,
	pordsdescomercial: string | null,
	recibos: IRecibosArr[],
	recibos_admon: IRecibosArr[],
	notas_credito: INotasData[], // dsvalor,dsnaturaleza,dsfactura,dsnumero,dsfecha, dscom AS descripcion, radicado
	notas_d_credito: INotasDC[],
	notas_c_ia: INotasIAData[],
	saldo: number,
	tipofactura: number,
	totalRecibos: number,
	totalvalorcop: number,
	usuariocausal: string | null,
}
export interface IFacturasTotal {
	sinVencer: number,
	subTotal: number,
	totalFacturado: number,
	vencido: number
}
export interface INotasRecibosTotal {
	totalRecibos: number,
	totalReciboAdm: number,
	totalNotasC: number,
	totalNotasDC: number,
	totalNotasC_IA: number,
}
export interface IFacturasResp {
	data: IFacturasData[],
	message: string
	status: "ok" | "error"
	totales: IFacturasTotal
}

export interface IRecibosData {
	dsvalor: number,
	dsfecha: string,
	dsfactura: string,
	dsnumero: string,
	tiporecibo: string,
}
export interface INotasIAData {
	dsvalor: number,
	dsnaturaleza: number,
	dsfactura: string,
	dsnumero: string,
	dsfecha: string,
	descripcion: string,
	tipo?: string,
}

export interface INotasData extends INotasIAData {
	radicado: string,
}

export interface INotasDC {
	dsvalor: number, 
	dsfactura: string,
}

export interface INotasRecibosData {
	recibos: IRecibosData[],
	recibos_admon: IRecibosData[],
	notas_credito: INotasData[],
	notas_d_credito: INotasDC[],
	notas_c_ia: INotasIAData[],
}
export interface INotasRecibosResp extends INotasRecibosData {
	message: string,
	status: "ok" | "error",
	totales: INotasRecibosTotal,
}

export interface ICostosAproxData {
	flete: number,
	costorecaudo: number,
	valorrecaudo: number,
}
export interface ICostosAproxResp {
	status: string, 
	message: string,
	data: ICostosAproxData
}

export interface IDataFacturaCSV {
	castigada: string,
	cliente: string,
	nit: string,
	observaciones: string, 
	ven: string, 
	factura: string, 
	fecha_factura: string, 
	fecha_venc: string,
	dias: number, 
	valor: number,
	fecha_abon1: string, 
	valor_abon1: number | null,
	fecha_abon2: string, 
	valor_abon2: number | null,
	fecha_abon3: string, 
	valor_abon3: number | null,
	fecha_abon4: string, 
	valor_abon4: number | null,
	fecha_nc?: string,
	valor_nc: number | null, 
	saldo: number
}

