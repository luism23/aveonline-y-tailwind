
export type ButtonTypes = {
    type: 'button' | 'submit' | 'reset';
}

export  interface DateInputs  {
    final: string;
    init: string;
}

export interface TokenPayload {
    aud: string;
    data: string;
    exp: number;
    i_idusuario: string;
    iss: string;
    usuario: string;
    enterprise: string;
}

export interface Base64 {
    blob: File,
    image: string,
    base: ArrayBuffer | string | null;
}
export interface Base64Req {
    base: ArrayBuffer | string | null ;
    type: string,
    name: string,
    ext: string,
}

export interface IUserData {
    id_user: string;
    id_enterprise: string;
    userName: string;
}

export type TypeIcon = "" | "ok" | "error" | "alert";

export interface IDataModal {
    show: boolean,
    title: string,
    message: string,
    type: TypeIcon
}

export interface IConfirmAccount {
    identificacionBeneficiario: number | null,
    nombreDelBeneficiario: string | null,
    bancoCuentaDelBeneficiarioDestino: number | null,
    numeroCuentaDelBeneficiario: string | null,
    tipoDeTransaccion: number | null,
    tipoDeDocumentoDeIdentificacion: number | null
}
export interface IConfirmAccountForm extends IConfirmAccount {
    accountNumber: string | null,
}

export interface ISaveAccountBankBody {
    identificacionBeneficiario: number;
    nombreDelBeneficiario: string;
    bancoCuentaDelBeneficiarioDestino: number;
    numeroCuentaDelBeneficiario: string;
    tipoDeTransaccion: number;
    tipoDeDocumentoDeIdentificacion: number;
}

export interface ISaveAccountBank {
    code: string,
    bodyToken: string,
    // body: Body[];
    body: ISaveAccountBankBody[]
}

export interface IResqGetlisBank {
    type_code: 1 | 2 ,
    bank: string
}

export interface ILisBankResp {
    dsnombre: string, 
    idcodbanco: number
}
export interface IArrayMultiSelect {
    label: string,
    value: number
}

export interface IExportData {
    data:IConfirmAccount, 
    bancos:IArrayMultiSelect[]
}

export interface IRespGeneAve {
    message: string,
    status: "ok" | "error"
}

export interface IAveTransp {
    id: number,
    text: string    
}
export interface IRespGeneAveTransp {
    message: string,
    status: "ok" | "error",
    transportadoras: IAveTransp[],
}
export interface IRespGeneAveData {
    message: string,
    data: string,
    status: "ok" | "error"
}
export interface IRespGetInvoice {
    base:  string,
    name:  string,
    status: "ok"
}

// eslint-disable-next-line @typescript-eslint/no-explicit-any
export type TypeRowCSV = object | any;

export interface IDatosRemintente {
    ciudad: string,
    ciudadCliente: string,
    codDepartamento: string,
    codPostalCliente: string,
    correo1: string,
    correo2: string,
    direccion: string,
    dsemail_responsable: string,
    idcliente: number;
    idsector: number,
    nombre: string,
    tel1: string,
    tel2: string,
    tel3: string,
    tel4: string,
    tipo_identificacion: number,
}

export interface IDatosRemintenteResp {
    cliente: IDatosRemintente[],
    message: string,
    status: "ok" | "error"
}

export interface IPostNotificarPago {
    tipo: "insertar";
    idexp: number;
    dsvalor: number;
    dsfecha_pago: string;
    dsconsec: string;
    dsidentificacion: string;
    idmedio_pago: number;
    pkid: string;
    pkids: string;
    dsfacturas: string;
    guiacourier: string;
    observaciones: string;
    file: Base64Req;
}

export interface IGetRefWompyReq {
	tipo: "solicitarReferencia",
	idexp : string,
	id_sector : number,
	valor_ave: string,
	facturas: string
}

export interface IGetResultPayWompi {
    tipo: "getResposeId",
    id_wompi: string,
    env_test?: boolean 
}
export interface IResultPayWompiResp extends IRespGeneAve { 
    data: [{
        numero_factura: string,
        status: string,
        total_wompi: string,
    }]
}


export type TypeIconSort = "sort" | "sort-down" | "sort-up";



