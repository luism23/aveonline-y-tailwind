

type AccountOpctions = {
    fecha: string,
    descripcion: string,
    ingreso: number,
    egreso: number,
    saldo_factura: number,
    saldo_total?: number
}

interface AccountState  {
    index?: number,
    id_factura: string,
    fecha: string,
    descripcion: string,
    ingreso: number,
    egreso: number,
    saldo_factura: number,
    saldo_total?: number;
    opcionales?: AccountOpctions[]
}

export { AccountState }
