
export interface ISendMsg {
  enterprise: string,
  userEnterprise: string
}

export interface IResp_SendMsg {
  status: string,
  bodyToken: string,
  exp: number,
  phone: string,
  mail: string
}

export interface IBankPayDataAccount {
  identificacionBeneficiario: string,
  nombreDelBeneficiario: string,
  bancoCuentaDelBeneficiarioDestino: string,
  numeroCuentaDelBeneficiario: string,
  tipoDeTransaccion: number,
  valorTransaccion: string,
  tipoDeDocumentoDeIdentificacion: number,
  oficinaDeEntrega?: string,
  email?: string,
}

export interface IBankSendPays {
  code: string,
	bodyToken: string,
  body: IBankPayDataAccount[]
}

export interface IFavoriteAccount {
  body: [
    {
      codigoConfirmacion: number,
      bancoCuentaDelBeneficiarioDestino: string,
      numeroCuentaDelBeneficiario: string,
      bodyToken: string
    }
  ]
  
}

export interface IBankSusbcriptDataAccount {
  identificacionBeneficiario: string,
  nombreDelBeneficiario: string,
  bancoCuentaDelBeneficiarioDestino: string,
  numeroCuentaDelBeneficiario: string,
  tipoDeTransaccion: number,
  tipoDeDocumentoDeIdentificacion: number,
}


export interface IBankSendSusbcripts {
  body: IBankSusbcriptDataAccount[]
}

export interface IRespListSusbcripts {
  identificacionBeneficiario: string,
  nombreDelBeneficiario: string,
  tipoDeDocumentoDeIdentificacion: number,
  numeroCuentaDelBeneficiario: number,
  tipoDecuenta: number,
  nombreBanco: string,
  bancoCuentaDelBeneficiarioDestino: string,
  cuentaPorDefecto: boolean,
}
