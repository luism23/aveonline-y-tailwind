import { IResponseAllDataRecaudos } from "./Recaudos"

export interface IResponseAveRecaudos {
    status: string;
    data: IResponseAllDataRecaudos;
}