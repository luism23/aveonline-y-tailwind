const itemsResumen = {
  saldo_actual: 2500000,
  recuados_proceso: 250000,
  costos: -50000,
  saldo_provision: 2700000,
  data:   [
    {
      title: 'Recargas billetera',
      tipo: 'POSITIVO',
      value: 0
    },
    {
      title: 'Dineros recaudados',
      tipo: 'POSITIVO',
      value: 1200000
    },
    {
      title: 'Ventas de productos',
      tipo: 'POSITIVO',
      value: 1300000
    },
    {
      title: 'Facturas',
      tipo: 'NEGATIVOS',
      value: 252000
    },
    {
      title: 'Indenizaciones',
      tipo: 'POSITIVO',
      value: 0
    },
  ]
};


export { itemsResumen };