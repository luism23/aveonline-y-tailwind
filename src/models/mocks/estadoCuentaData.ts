import { AccountState }  from '@/models/types/index';

const ESTADO_CUENTA: AccountState[] = [
 
    {
        index:0,
        id_factura: 'a-000-2213',
        fecha: '03/16/2022',
        descripcion: 'no Sobornos',
        ingreso: 0,
        egreso: 8000,
        saldo_factura: 800000,
        saldo_total: -235600,
        opcionales: [
            {
                fecha: '03/16/2022',
                descripcion: 'detalle 1',
                ingreso: 0,
                egreso: 800,
                saldo_factura: 8000
            },
            {
                fecha: '03/16/2022',
                descripcion: 'detalle 2',
                ingreso: 0,
                egreso: 900,
                saldo_factura: 8800
            }
        ]

    },
    {
        index:1,
        id_factura: 'c-123-7777',
        fecha: '03/16/2022',
        descripcion: 'Desembolso de dinero',
        ingreso: 2000,
        egreso: 0,
        saldo_factura: 0,
        saldo_total: 20200,
        opcionales: [
            {
                fecha: '03/16/2022',
                descripcion: 'detallex 1',
                ingreso: 0,
                egreso: 700,
                saldo_factura: 9000
            },
            {
                fecha: '03/16/2022',
                descripcion: 'detalle 2',
                ingreso: 0,
                egreso: 900,
                saldo_factura: 8800
            }
        ]

    },
    {
        index:2,
        id_factura: 'a-123-2213',
        fecha: '03/16/2022',
        descripcion: 'Pago de factura electronica',
        ingreso: 2452000,
        egreso: 0,
        saldo_factura: 2500000,
        saldo_total: 2452000 - 0,
        opcionales: [
            {
                fecha: '03/16/2022',
                descripcion: 'detallex 1',
                ingreso: 0,
                egreso: 700,
                saldo_factura: 9000
            },
            {
                fecha: '03/16/2022',
                descripcion: 'detalle 2',
                ingreso: 0,
                egreso: 900,
                saldo_factura: 8800
            }
        ]

    },
    {
        index:3,
        id_factura: 'b-123-4567',
        fecha: '03/18/2022',
        descripcion: 'Factura Transporte nacional',
        ingreso: 1000000,
        egreso: 0,
        saldo_factura: 1450000,
        saldo_total: 1000000 - 0,
        opcionales: [
            {
                fecha: '03/16/2022',
                descripcion: 'detalle 1',
                ingreso: 0,
                egreso: 800,
                saldo_factura: 8000
            },
            {
                fecha: '03/16/2022',
                descripcion: 'detalle 2',
                ingreso: 0,
                egreso: 900,
                saldo_factura: 8800
            }
        ]

    },
    {
        index:4,
        id_factura: 'a-123-3344',
        fecha: '04/17/2022',
        descripcion: 'solion poductos',
        ingreso: 2000,
        egreso: 100,
        saldo_factura: 250,
        saldo_total: 2000 - 100
    },{
        index:5,
        id_factura: 'a-000-2213',
        fecha: '03/16/2022',
        descripcion: 'Sobornos',
        ingreso: 0,
        egreso: 800000,
        saldo_factura: 800000,
        saldo_total: 0 - 800000,
        opcionales: [
            {
                fecha: '03/16/2022',
                descripcion: 'detalle 1',
                ingreso: 0,
                egreso: 800,
                saldo_factura: 8000
            },
            {
                fecha: '03/16/2022',
                descripcion: 'detalle 2',
                ingreso: 0,
                egreso: 900,
                saldo_factura: 8800
            }
        ]

    },
    {
        index:6,
        id_factura: 'c-123-7777',
        fecha: '03/16/2022',
        descripcion: 'Desembolso de dinero',
        ingreso: 2000,
        egreso: 0,
        saldo_factura: 0,
        saldo_total: 2000 - 0,
        opcionales: [
            {
                fecha: '03/16/2022',
                descripcion: 'detallex 1',
                ingreso: 0,
                egreso: 700,
                saldo_factura: 9000
            },
            {
                fecha: '03/16/2022',
                descripcion: 'detalle 2',
                ingreso: 0,
                egreso: 900,
                saldo_factura: 8800
            }
        ]

    },
    {
        index:7,
        id_factura: 'a-123-2213',
        fecha: '03/16/2022',
        descripcion: 'Pago de factura electronica',
        ingreso: 2452000,
        egreso: 0,
        saldo_factura: 2500000

    },
    {
        index:8,
        id_factura: 'b-123-4567',
        fecha: '03/18/2022',
        descripcion: 'Factura Transporte nacional',
        ingreso: 1000000,
        egreso: 0,
        saldo_factura: 1450000,
        opcionales: [
            {
                fecha: '03/16/2022',
                descripcion: 'detallex 1',
                ingreso: 0,
                egreso: 700,
                saldo_factura: 9000
            },
            {
                fecha: '03/16/2022',
                descripcion: 'detalle 2',
                ingreso: 0,
                egreso: 900,
                saldo_factura: 8800
            }
        ]

    },
    {
        index:9,
        id_factura: 'a-123-3344',
        fecha: '04/17/2022',
        descripcion: 'solion poductos',
        ingreso: 2000,
        egreso: 100,
        saldo_factura: 250

    },{
        index:10,
        id_factura: 'a-000-2213',
        fecha: '03/16/2022',
        descripcion: 'casi Sobornos',
        ingreso: 0,
        egreso: 800000,
        saldo_factura: 800000

    },
    {
        index:11,
        id_factura: 'c-123-7777',
        fecha: '03/16/2022',
        descripcion: 'Desembolso de dinero',
        ingreso: 2000,
        egreso: 0,
        saldo_factura: 0

    },
    {
        index:12,
        id_factura: 'a-123-2213',
        fecha: '03/16/2022',
        descripcion: 'Pago de factura electronica',
        ingreso: 2452000,
        egreso: 0,
        saldo_factura: 2500000

    },
    {
        index:13,
        id_factura: 'b-123-4567',
        fecha: '03/18/2022',
        descripcion: 'Factura Transporte nacional',
        ingreso: 1000000,
        egreso: 0,
        saldo_factura: 1450000

    },
    {
        index:14,
        id_factura: 'a-123-3344',
        fecha: '04/17/2022',
        descripcion: 'solion poductos',
        ingreso: 2000,
        egreso: 100,
        saldo_factura: 250

    },{
        index:15,
        id_factura: 'a-000-2213',
        fecha: '03/16/2022',
        descripcion: 'Sobornos',
        ingreso: 0,
        egreso: 800000,
        saldo_factura: 800000

    },
    {
        index:16,
        id_factura: 'c-123-7777',
        fecha: '03/16/2022',
        descripcion: 'Desembolso de dinero',
        ingreso: 2000,
        egreso: 0,
        saldo_factura: 0

    },
    {
        index:17,
        id_factura: 'a-123-2213',
        fecha: '03/16/2022',
        descripcion: 'Pago de factura electronica',
        ingreso: 2452000,
        egreso: 0,
        saldo_factura: 2500000

    },
    {
        index:18,
        id_factura: 'b-123-4567',
        fecha: '03/18/2022',
        descripcion: 'Factura Transporte nacional',
        ingreso: 1000000,
        egreso: 0,
        saldo_factura: 1450000

    },
    {
        index:19,
        id_factura: 'a-123-3344',
        fecha: '04/17/2022',
        descripcion: 'solion poductos',
        ingreso: 2000,
        egreso: 100,
        saldo_factura: 250

    },{
        index:20,
        id_factura: 'a-000-2213',
        fecha: '03/16/2022',
        descripcion: 'ee siSobornos',
        ingreso: 0,
        egreso: 800000,
        saldo_factura: 800000

    },
    {
        index:21,
        id_factura: 'c-123-7777',
        fecha: '03/16/2022',
        descripcion: 'Desembolso de dinero',
        ingreso: 2000,
        egreso: 0,
        saldo_factura: 0

    },
    {
        index:22,
        id_factura: 'a-123-2213',
        fecha: '03/16/2022',
        descripcion: 'Pago de factura electronica',
        ingreso: 2452000,
        egreso: 0,
        saldo_factura: 2500000

    },
    {
        index:22,
        id_factura: 'b-123-4567',
        fecha: '03/18/2022',
        descripcion: 'Factura Transporte nacional',
        ingreso: 1000000,
        egreso: 0,
        saldo_factura: 1450000

    },
    {
        index:23,
        id_factura: 'a-123-3344',
        fecha: '04/17/2022',
        descripcion: 'solion poductos',
        ingreso: 2000,
        egreso: 100,
        saldo_factura: 250

    },{
        index:24,
        id_factura: 'a-000-2213',
        fecha: '03/16/2022',
        descripcion: 'Sobornos',
        ingreso: 0,
        egreso: 800000,
        saldo_factura: 800000

    },
    {
        index:25,
        id_factura: 'c-123-7777',
        fecha: '03/16/2022',
        descripcion: 'Desembolso de dinero',
        ingreso: 2000,
        egreso: 0,
        saldo_factura: 0

    },
    {
        id_factura: 'a-123-2213',
        fecha: '03/16/2022',
        descripcion: 'Pago de factura electronica',
        ingreso: 2452000,
        egreso: 0,
        saldo_factura: 2500000

    },
    {
        id_factura: 'b-123-4567',
        fecha: '03/18/2022',
        descripcion: 'Factura Transporte nacional',
        ingreso: 1000000,
        egreso: 0,
        saldo_factura: 1450000

    },
    {
        id_factura: 'a-123-3344',
        fecha: '04/17/2022',
        descripcion: 'solion poductos',
        ingreso: 2000,
        egreso: 100,
        saldo_factura: 250

    },{
        id_factura: 'a-000-2213',
        fecha: '03/16/2022',
        descripcion: 'Sobornos',
        ingreso: 0,
        egreso: 800000,
        saldo_factura: 800000

    },
    {
        id_factura: 'c-123-7777',
        fecha: '03/16/2022',
        descripcion: 'Desembolso de dinero',
        ingreso: 2000,
        egreso: 0,
        saldo_factura: 0

    },
    {
        id_factura: 'a-123-2213',
        fecha: '03/16/2022',
        descripcion: 'Pago de factura electronica',
        ingreso: 2452000,
        egreso: 0,
        saldo_factura: 2500000

    },
    {
        id_factura: 'b-123-4567',
        fecha: '03/18/2022',
        descripcion: 'Factura Transporte nacional',
        ingreso: 1000000,
        egreso: 0,
        saldo_factura: 1450000

    },
    {
        id_factura: 'a-123-3344',
        fecha: '04/17/2022',
        descripcion: 'solion poductos',
        ingreso: 2000,
        egreso: 100,
        saldo_factura: 250

    },{
        id_factura: 'a-000-2213',
        fecha: '03/16/2022',
        descripcion: 'Sobornos',
        ingreso: 0,
        egreso: 800000,
        saldo_factura: 800000

    },
    {
        id_factura: 'c-123-7777',
        fecha: '03/16/2022',
        descripcion: 'Desembolso de dinero',
        ingreso: 2000,
        egreso: 0,
        saldo_factura: 0

    },
    {
        id_factura: 'a-123-2213',
        fecha: '03/16/2022',
        descripcion: 'Pago de factura electronica',
        ingreso: 2452000,
        egreso: 0,
        saldo_factura: 2500000

    },
    {
        id_factura: 'b-123-4567',
        fecha: '03/18/2022',
        descripcion: 'Factura Transporte nacional',
        ingreso: 1000000,
        egreso: 0,
        saldo_factura: 1450000

    },
    {
        id_factura: 'a-123-3344',
        fecha: '04/17/2022',
        descripcion: 'solion poductos',
        ingreso: 2000,
        egreso: 100,
        saldo_factura: 250

    },{
        id_factura: 'a-000-2213',
        fecha: '03/16/2022',
        descripcion: 'Sobornos',
        ingreso: 0,
        egreso: 800000,
        saldo_factura: 800000

    },
    {
        id_factura: 'c-123-7777',
        fecha: '03/16/2022',
        descripcion: 'Desembolso de dinero',
        ingreso: 2000,
        egreso: 0,
        saldo_factura: 0

    },
    {
        id_factura: 'a-123-2213',
        fecha: '03/16/2022',
        descripcion: 'Pago de factura electronica',
        ingreso: 2452000,
        egreso: 0,
        saldo_factura: 2500000

    },
    {
        id_factura: 'b-123-4567',
        fecha: '03/18/2022',
        descripcion: 'Factura Transporte nacional',
        ingreso: 1000000,
        egreso: 0,
        saldo_factura: 1450000

    },
    {
        id_factura: 'a-123-3344',
        fecha: '04/17/2022',
        descripcion: 'solion poductos',
        ingreso: 2000,
        egreso: 100,
        saldo_factura: 250

    },{
        id_factura: 'a-000-2213',
        fecha: '03/16/2022',
        descripcion: 'Sobornos',
        ingreso: 0,
        egreso: 800000,
        saldo_factura: 800000

    },
    {
        id_factura: 'c-123-7777',
        fecha: '03/16/2022',
        descripcion: 'Desembolso de dinero',
        ingreso: 2000,
        egreso: 0,
        saldo_factura: 0

    },
    {
        id_factura: 'a-123-2213',
        fecha: '03/16/2022',
        descripcion: 'Pago de factura electronica',
        ingreso: 2452000,
        egreso: 0,
        saldo_factura: 2500000

    },
    {
        id_factura: 'b-123-4567',
        fecha: '03/18/2022',
        descripcion: 'Factura Transporte nacional',
        ingreso: 1000000,
        egreso: 0,
        saldo_factura: 1450000

    },
    {
        id_factura: 'a-123-3344',
        fecha: '04/17/2022',
        descripcion: 'solion poductos',
        ingreso: 2000,
        egreso: 100,
        saldo_factura: 250

    },{
        id_factura: 'a-000-2213',
        fecha: '03/16/2022',
        descripcion: 'Sobornos',
        ingreso: 0,
        egreso: 800000,
        saldo_factura: 800000

    },
    {
        id_factura: 'c-123-7777',
        fecha: '03/16/2022',
        descripcion: 'Desembolso de dinero',
        ingreso: 2000,
        egreso: 0,
        saldo_factura: 0

    },
    {
        id_factura: 'a-123-2213',
        fecha: '03/16/2022',
        descripcion: 'Pago de factura electronica',
        ingreso: 2452000,
        egreso: 0,
        saldo_factura: 2500000

    },
    {
        id_factura: 'b-123-4567',
        fecha: '03/18/2022',
        descripcion: 'Factura Transporte nacional',
        ingreso: 1000000,
        egreso: 0,
        saldo_factura: 1450000

    },
    {
        id_factura: 'a-123-3344',
        fecha: '04/17/2022',
        descripcion: 'solion poductos',
        ingreso: 2000,
        egreso: 100,
        saldo_factura: 250

    },{
        id_factura: 'a-000-2213',
        fecha: '03/16/2022',
        descripcion: 'Sobornos',
        ingreso: 0,
        egreso: 800000,
        saldo_factura: 800000

    },
    {
        id_factura: 'c-123-7777',
        fecha: '03/16/2022',
        descripcion: 'Desembolso de dinero',
        ingreso: 2000,
        egreso: 0,
        saldo_factura: 0

    },
    {
        id_factura: 'a-123-2213',
        fecha: '03/16/2022',
        descripcion: 'Pago de factura electronica',
        ingreso: 2452000,
        egreso: 0,
        saldo_factura: 2500000

    },
    {
        id_factura: 'b-123-4567',
        fecha: '03/18/2022',
        descripcion: 'Factura Transporte nacional',
        ingreso: 1000000,
        egreso: 0,
        saldo_factura: 1450000

    },
    {
        id_factura: 'a-123-3344',
        fecha: '04/17/2022',
        descripcion: 'solion poductos',
        ingreso: 2000,
        egreso: 100,
        saldo_factura: 250

    },{
        id_factura: 'a-000-2213',
        fecha: '03/16/2022',
        descripcion: 'Sobornos',
        ingreso: 0,
        egreso: 800000,
        saldo_factura: 800000

    },
    {
        id_factura: 'c-123-7777',
        fecha: '03/16/2022',
        descripcion: 'Desembolso de dinero',
        ingreso: 2000,
        egreso: 0,
        saldo_factura: 0

    },
    {
        id_factura: 'a-123-2213',
        fecha: '03/16/2022',
        descripcion: 'Pago de factura electronica',
        ingreso: 2452000,
        egreso: 0,
        saldo_factura: 2500000

    },
    {
        id_factura: 'b-123-4567',
        fecha: '03/18/2022',
        descripcion: 'Factura Transporte nacional',
        ingreso: 1000000,
        egreso: 0,
        saldo_factura: 1450000

    },
    {
        id_factura: 'a-123-3344',
        fecha: '04/17/2022',
        descripcion: 'solion poductos',
        ingreso: 2000,
        egreso: 100,
        saldo_factura: 250

    },{
        id_factura: 'a-000-2213',
        fecha: '03/16/2022',
        descripcion: 'Sobornos',
        ingreso: 0,
        egreso: 800000,
        saldo_factura: 800000

    },
    {
        id_factura: 'c-123-7777',
        fecha: '03/16/2022',
        descripcion: 'Desembolso de dinero',
        ingreso: 2000,
        egreso: 0,
        saldo_factura: 0

    },
    {
        id_factura: 'a-123-2213',
        fecha: '03/16/2022',
        descripcion: 'Pago de factura electronica',
        ingreso: 2452000,
        egreso: 0,
        saldo_factura: 2500000

    },
    {
        id_factura: 'b-123-4567',
        fecha: '03/18/2022',
        descripcion: 'Factura Transporte nacional',
        ingreso: 1000000,
        egreso: 0,
        saldo_factura: 1450000

    },
    {
        id_factura: 'a-123-3344',
        fecha: '04/17/2022',
        descripcion: 'solion poductos',
        ingreso: 2000,
        egreso: 100,
        saldo_factura: 250

    },
]

export { ESTADO_CUENTA }