
import { IRecaudosData } from "@/models/types";
import { defineStore } from "pinia";
import { ref } from "vue";

type TiposValores = "arecaudar" | "liquidadas_billetera" | "devoluciones" | "indenizaciones" | "procesoindem" | "recaudadas" | "liquidadas" ;

export const useRecuadosStore = defineStore('recuados', () => {
    const total_arecaudar               = ref<number>(0);
    const total_devoluciones            = ref<number>(0);
    const total_indenizaciones          = ref<number>(0);
    const total_procesoindem            = ref<number>(0);
    const total_recaudadas              = ref<number>(0);
    const total_liquidadas              = ref<number>(0);
    const total_liquidadas_billetera    = ref<number>(0);

    const data_arecaudar            = ref<IRecaudosData[]>([]);
    const data_devolucion           = ref<IRecaudosData[]>([]);
    const data_indemnizado          = ref<IRecaudosData[]>([]);
    const data_liquidadas           = ref<IRecaudosData[]>([]);
    const data_recaudadas           = ref<IRecaudosData[]>([]);
    const data_procesoindem         = ref<IRecaudosData[]>([]);

    const setterValues = (tipo: TiposValores) => {
        return {
            mutation(value: number){
                switch (tipo) {
                    case 'arecaudar':
                        total_arecaudar.value = value;
                        break;
                    case 'devoluciones':
                        total_devoluciones.value = value;
                        break;
                    case 'indenizaciones':
                        total_indenizaciones.value = value;
                        break;
                    case 'procesoindem':
                        total_procesoindem.value = value;
                        break;
                    case 'recaudadas':                        
                        total_recaudadas.value = value;
                        break;
                    case 'liquidadas':
                        total_liquidadas.value = value;
                        break;
                    case 'liquidadas_billetera':
                        total_liquidadas_billetera.value = value;
                        break;
                }
            }
        }
    }

    const setterData = (tipo: TiposValores) => {
        return {
            mutation(data: IRecaudosData[]) {
                switch (tipo) {
                    case 'arecaudar':
                        data_arecaudar.value = data;
                        break;
                    case 'devoluciones':
                        data_devolucion.value = data;
                        break;
                    case 'indenizaciones':
                        data_indemnizado.value = data;
                        break;
                    case 'procesoindem':
                        data_procesoindem.value = data;
                        break;
                    case 'recaudadas':
                        data_recaudadas.value = data;
                        break;
                    case 'liquidadas':
                        data_liquidadas.value = data;
                        break;
                }
            }
        }
    }


    return {
        total_arecaudar,
        total_devoluciones,
        total_indenizaciones,
        total_procesoindem,
        total_recaudadas,
        total_liquidadas,
        total_liquidadas_billetera,

        data_arecaudar,
        data_devolucion,
        data_indemnizado,
        data_liquidadas,
        data_recaudadas,
        data_procesoindem,

        setterValues,
        setterData
    }


})