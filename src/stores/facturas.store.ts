
import { IFacturasData, IFacturasTotal } from "@/models/types";
import { defineStore } from "pinia";
import { ref } from "vue";


export const useFacturasStore = defineStore('facturas', () => {
  const sin_vencer        = ref<number>(0);
  const sub_total         = ref<number>(0);
  const total_facturado   = ref<number>(0);
  const vencido           = ref<number>(0);
  const dataFacturas      = ref<IFacturasData[]>([])
  
  const setterTotales = (totales: IFacturasTotal): void => {
    sin_vencer.value      = totales.sinVencer;
    sub_total.value       = totales.subTotal;
    total_facturado.value = totales.totalFacturado;
    vencido.value         = totales.vencido;
  }
  
  const setterFacturas = (data: IFacturasData[]) => {
    dataFacturas.value = data;
  }

  const getterTotales = (): IFacturasTotal => {
    return {
      sinVencer:      sin_vencer.value,
      subTotal:       sub_total.value,
      totalFacturado: total_facturado.value,
      vencido:        vencido.value
    }
  }

  const getterFacturas = (): IFacturasData[] => {
    return dataFacturas.value;
  }
  const getterFacturasPendientes = (): IFacturasData[] => {
    const facturas:IFacturasData[] = [];
    dataFacturas.value.forEach(el => {
      if (el.saldo > 0) {
        facturas.push(el);
      }
    });
    return facturas;
    
  }

  return {
    setterTotales,
    getterTotales,
    getterFacturas,
    setterFacturas,
    getterFacturasPendientes
  }


})