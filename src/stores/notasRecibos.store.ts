import { ICostosAproxData, INotasData, INotasDC, INotasIAData, INotasRecibosData, INotasRecibosTotal, IRecibosData } from "@/models/types";
import { defineStore } from "pinia";
import { ref } from "vue";

export const useNotasRecibosStore = defineStore('notasRecibos', () => {

  const totalRecibos         = ref<number>(0);
  const totalReciboAdm       = ref<number>(0);
  const totalNotasC          = ref<number>(0);
  const totalNotasDC         = ref<number>(0);
  const totalNotasC_IA       = ref<number>(0);

  const costosTotalFlete            = ref<number>(0);
  const costosTotalCostorecaudo     = ref<number>(0);
  const costosTotalValorrecaudo     = ref<number>(0);

  const dataRecibos         =  ref<IRecibosData[]>([]);
	const dataRecibos_admon   =  ref<IRecibosData[]>([]);
	const dataNotas_credito   =  ref<INotasData[]>([]);
	const dataNotas_d_credito =  ref<INotasDC[]>([]);
	const dataNotas_c_ia      =  ref<INotasIAData[]>([]);

  const setterTotales = (totales :INotasRecibosTotal): void => {
    totalRecibos.value =    totales.totalRecibos;
    totalReciboAdm.value =  totales.totalReciboAdm;
    totalNotasC.value =     totales.totalNotasC;
    totalNotasDC.value =    totales.totalNotasDC;
    totalNotasC_IA.value =  totales.totalNotasC_IA;
  }

  const setterCostosTotales = (costos: ICostosAproxData): void => {
    costosTotalFlete.value = costos.flete;
    costosTotalCostorecaudo.value = costos.costorecaudo;
    costosTotalValorrecaudo.value = costos.valorrecaudo;
  }

  const setterDataRecibos = ( allRecibos: INotasRecibosData ): void => {
    dataRecibos.value         = allRecibos.recibos;
    dataRecibos_admon.value   = allRecibos.recibos_admon;
    dataNotas_credito.value   = allRecibos.notas_credito;
    dataNotas_d_credito.value = allRecibos.notas_d_credito;
    dataNotas_c_ia.value      = allRecibos.notas_c_ia;
  }
  
  const getterTotales = (): INotasRecibosTotal => {
    return {
      totalRecibos: totalRecibos.value,
      totalReciboAdm: totalReciboAdm.value,
      totalNotasC: totalNotasC.value,
      totalNotasDC: totalNotasDC.value,
      totalNotasC_IA: totalNotasC_IA.value,
    }
  }
  const getterCostosTotales = (): ICostosAproxData => {
    return {
      flete: costosTotalFlete.value,
      costorecaudo: costosTotalCostorecaudo.value,
      valorrecaudo: costosTotalValorrecaudo.value
    }
  }

  const getterAllData = () => {
    return {
      dataRecibos: dataRecibos.value,
      dataRecibos_admon: dataRecibos_admon.value,
      dataNotas_credito: dataNotas_credito.value,
      dataNotas_d_credito: dataNotas_d_credito.value,
      dataNotas_c_ia: dataNotas_c_ia.value,
    }
  }

  return {
    getterTotales,
    setterTotales,

    setterCostosTotales,
    getterCostosTotales,

    setterDataRecibos,
    getterAllData
  }

})