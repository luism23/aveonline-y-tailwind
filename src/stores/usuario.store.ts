import { defineStore } from "pinia";
import { IDatosRemintente, IRespListSusbcripts, IUserData } from "@/models/types";

export const useUsuarioStore = defineStore("usuario", {
  state: () => ({
    id_user: <string>"",
    id_enterprise: <string>"",
    userName: <string>"",
    listAccounts: <IRespListSusbcripts[]>[],
    availableForPayout: <number> 0,

    datosRemitente: <IDatosRemintente[]>[]

  }),
  getters: {
    getUser(state) {
      return state.userName;
    },
    getIdUser(state) {
      return state.id_user;
    },
    getIdEnterprise(state) {      
      return state.id_enterprise;
    },
    getAccounts(state) {
      console.log(state.listAccounts);
      return state.listAccounts;
    },
    getPayOut(state) {
      return state.availableForPayout;
    },
    getDatosRemitente(state) {
      return state.datosRemitente;
    }
  },
  actions: {
    addDataUser(dataBasic: IUserData) {      
      this.id_user = dataBasic.id_user;
      this.id_enterprise = dataBasic.id_enterprise;
      this.userName = dataBasic.userName;      
    },
    addListAccounts( list:IRespListSusbcripts[] ) {
      console.log(list);
      this.listAccounts = list;
    },
    sumPayOut(value: number) {
      this.availableForPayout = this.availableForPayout + value
    },
    resPayOut(value: number) {
      this.availableForPayout = this.availableForPayout - value
    },
    resetValuePayOut() {
      this.availableForPayout = 0
    },
    saveDatosRemitente(data: IDatosRemintente[]) {
      this.datosRemitente = data;
    }
    
  },
});
