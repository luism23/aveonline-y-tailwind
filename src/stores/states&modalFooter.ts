import { defineStore } from "pinia";
type ChangeToogle = "" | "false" | "true";

export const useStatesModalStore = defineStore("statesModal", {
  state: () => ({
    toggleWalletRecharge: <boolean>false,
    toggleMoneyCollected: <boolean>false,
    toggleInvoices:       <boolean>false,
    toggleCompensation:   <boolean>false,
    toggleAdvances:       <boolean>false,
    toggleCrediNotes:     <boolean>false,
    toggleWithDrawal:     <boolean>false,
  }),
  getters: {
    WalletRecharge(state) { return state.toggleWalletRecharge },
    MoneyCollected(state) { return state.toggleMoneyCollected },
    Invoices(state)       { return state.toggleInvoices },
    Compensation(state)   { return state.toggleCompensation },
    Advances(state)       { return state.toggleAdvances },
    CrediNotes(state)     { return state.toggleCrediNotes },
    WithDrawal(state)     { return state.toggleWithDrawal },
  },
  actions: {
    ToggleWalletRecharge(str: ChangeToogle = '') {
      if (str) {
        return this.toggleWalletRecharge = Boolean(str);
      }
      this.toggleWalletRecharge = !this.toggleWalletRecharge;
    },
    ToggleMoneyCollected(str: ChangeToogle = '') {
      if (str) {
        return this.toggleMoneyCollected = Boolean(str);
      }
      this.toggleMoneyCollected = !this.toggleMoneyCollected;
    },
    ToggleInvoices(str: ChangeToogle = '') {
      if (str) {
        return this.toggleInvoices = Boolean(str);
      }
      this.toggleInvoices = !this.toggleInvoices;
    },
    ToggleCompensation(str: ChangeToogle = '') {
      if (str) {
        return this.toggleCompensation = Boolean(str);
      }
      this.toggleCompensation = !this.toggleCompensation;
    },
    ToggleAdvances(str: ChangeToogle = '') {
      if (str) {
        return this.toggleAdvances = Boolean(str);
      }
      this.toggleAdvances = !this.toggleAdvances;
    },
    ToggleCrediNotes(str: ChangeToogle = '') {
      if (str) {
        return this.toggleCrediNotes = Boolean(str);
      }
      this.toggleCrediNotes = !this.toggleCrediNotes;
    },
    ToggleWithDrawal(str: ChangeToogle = '') {
      if (str) {
        return this.toggleWithDrawal = Boolean(str);
      }
      this.toggleWithDrawal = !this.toggleWithDrawal;
    },
  }
})