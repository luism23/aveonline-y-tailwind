
/**
 * Generador de una cadena de numeros random 
 * @param len tamaño de la cadena esperada
 * @param onlyNum si se desea solo numeros
 * @returns 
 */
const generateRandomNumber = (len = 18, onlyNum = true ) => {
  let text = '';
  const possible = onlyNum ? "0123456789" : "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789" ;
  for (let i = 0; i < len; i++) {
    text += possible.charAt(Math.floor(Math.random() * possible.length));
  }
  return text;
}
/**
 * Cubre un id antes de ser enviado por http
 * @param id_self id que se desea ocultar
 * @returns 
 */
export const coverUpIdUser = (id_self: string ): string=> {
  return id_self;
}
/**
 * Cubre un id antes de ser enviado por http
 * @param id_self id que se desea ocultar
 * @returns 
 */
export const coverUpIdEnterprise = (id_self: string ): string => {
  return id_self;
}


