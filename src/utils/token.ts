
import { TOKEN_NAME } from "@/constants";
import { TokenPayload } from "@/models/types/TypesGenerales";

/**
 * Parseador del toekn
 */
export const parseJWT = (token: string): TokenPayload | null => {
  try {
    return JSON.parse(atob(token.split('.')[1]));
  } catch (e) {
    return null;
  }
};

export const getLocalToken = ():string | null => {
    return localStorage.getItem(TOKEN_NAME);
}

