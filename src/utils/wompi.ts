import { IFacturasData } from "@/models/types";
import { modoDEV } from "@/services/http";

const regex = /[,.]/g;

/**Remueve el punto del valor con los decimales ya puestos */
const removeDotWompi = (numString: string) => {
  const NumString = `${numString}`;
  const resul = NumString.replace(regex, '');
  return Number(resul);
}

/**
 * Selecciona las facturas q esten seleccionadas/chackeadas
 * @param arrayFact 
 * @returns array de las facturas con prefijo y id
 */
export const selectInvoicesSelect = (arrayFact:IFacturasData[]) => {
  const factu:string[] = [];
  arrayFact.forEach(el => {
    const checkbox = document.getElementById(`check-${el.idpedido}`) as HTMLInputElement; 
    if (checkbox) {
      const check = checkbox.checked;
      if (check) {
        factu.push(`${el.dsprefijo}-${el.idpedido}`);
      }
    }
  })
  return factu;
}

/**
 * Selecionas las facturas q estan vencidas
 * @param arrayFact 
 * @returns 
 */
export const selectInvoicesOverdue = (arrayFact:IFacturasData[]) => {
  const factu:string[] = [];
  arrayFact.forEach(el => {
    const checkbox = document.getElementById(`check-${el.idpedido}`) as HTMLInputElement; 
    if (checkbox) {
      const check = checkbox.checked;
      if (check && el.dias_venc && el.dias_venc < 1 ) {
        factu.push(`${el.dsprefijo}-${el.idpedido}`);
      }
    }
  })
  return factu;
}

/**
 * Genera el direccionamiento a Wompi para el pago 
 * @param value valor a pagar con dos decimales ya incluidos
 * @param invoice referencia a pagar
 * @param test activar el modo de prueba
 */
export const goToWompi = ( value: string, invoice: string,  test = false ): void => {
  const valueWithCents = regex.test(value)? removeDotWompi(value) : Number(value) * 100;
  const key = test ? "pub_test_syWuNAWB0feEchNfZdqQSzEClvj60Y6i" : "pub_prod_FU1f2kodO1FXem7mpyvkIhFUHmTRLLN7";
  const ulrReturn = modoDEV ? "http://localhost:8080" :"https://aveonline.co/app/v2/";
  const url = `https://checkout.wompi.co/p/?public-key=${key}&currency=COP&amount-in-cents=${valueWithCents}`;
  const moreParams = `&reference=${invoice}&redirect-url=${ulrReturn}`;
  window.location.href = `${url}${moreParams}`;
};

