import { REGEX } from "@/constants";
import { Base64, IAveTransp } from "@/models/types";
type IOrd = "Desc" | "Asc";
interface ICurrency {
  value: number,
  fractionDigits:number,
  locales: "es-CO" | "en-US",
  currency: "COP" | "USD",
}
interface IOpcionCurrency {
  style?: string,
  currency: string,
  minimumFractionDigits: number
}

/**
 * Elimina caracteres extraños solo permitiendo alfanumericos.
 * Si el resultado no es alfanumerico retorna @type: false
 * @param value el valor a sanizar
 * @returns string | boolean
 */
export const sanitizerValues = (value: string): string | boolean => {
  const strangeCharacters = REGEX.CARACTERES_EXTRAÑOS;
  const allowedCharacters = REGEX.ALFA_NUM;
  let sanitized = value.replace(strangeCharacters, "");
  sanitized = sanitized.replace(/  +/g, ' ');
  if (allowedCharacters.test(sanitized)) {
    return sanitized;
  }
  return allowedCharacters.test(sanitized);
};

/**
 * Ordenar cualquier Objeto recibiendo el parametros por el que se quiera ordenar y el tipo de orden
 * @param data Cualquier arreglo
 * @param param el parametro por el que se quiera ordenar
 * @param ord Asc || Desc
 */
// eslint-disable-next-line @typescript-eslint/no-explicit-any
export const orderAny = ( data: any[], param: string, ord: IOrd = "Desc"): void => {
  if (ord == "Asc") {
    data.sort((a, b) => {
      if (Number(a[param]) && Number(b[param])) {
        return a[param] - b[param]
      }
      if (a[param] < b[param]) {
        return -1;
      }
      if (a[param] > b[param]) {
        return 1;
      }
      return 0;
    });
  } else if (ord == "Desc") {
    data.sort((a, b) => {
      if (Number(a[param]) && Number(b[param])) {
        return b[param] - a[param]
      }
      if (a[param] < b[param]) {
        return 1;
      }
      if (a[param] > b[param]) {
        return -1;
      }
      return 0;
    });
  }
};

/**
 * Elimina los elementos repetidos del array que se pase ne los parametros
 * @param arr array de tipo Any
 * @returns el array sin elementos repetidos
 */
// eslint-disable-next-line @typescript-eslint/no-explicit-any
export const removeDuplicates = (arr:Array<any>):Array<any> => {
  return arr.filter((valor, ind) => {
    return arr.indexOf(valor) === ind;
  });
}


/**
 * Convierte archivos File en base 64
 * @param file File de cualquier tipo
 */
export const base64 = async (file: File) =>
  new Promise<Base64>((resolve, reject) => {
    try {
      const image = window.URL.createObjectURL(file); // crea un DOMString que contiene una URL que representa al objeto pasado como parámetro.
      const reader = new FileReader();
      reader.readAsDataURL(file);
      reader.onload = () => {
        resolve({
          blob: file,
          image,
          base: reader.result,
        });
      };
      reader.onerror = () => {
        resolve({
          blob: file,
          image,
          base: null,
        });
      };
    } catch (err) {
      reject(err)
      return null;
    }
  });


/*** Dar el formato de dinero para wompi */
export const formatCurrentWompi = (value: string): number => {
  const valueSplit = value.split(".");
  return Number(valueSplit[0]) * 100;
}
/*** Dar el formato de dinero */
export const formatCurrency = ({
  value,
  fractionDigits = 0,
  locales = "es-CO",
  currency = "COP",
}:ICurrency): string => {
  const formatted = new Intl.NumberFormat(locales, {
    style: "currency",
    currency: currency,
    minimumFractionDigits: fractionDigits,
  }).format(value);
  return formatted;
}
/*** Dar el formato de dinero COP */
export const formatCurrencyCo = (number: number, fractionDigits = 0 ) => {
  const locales = 'es-CO';
  const currency = 'COP';
  const opcions: IOpcionCurrency = {
    style: 'currency',
    currency: currency,
    minimumFractionDigits: fractionDigits
  }
  if (number < 0) {
    delete opcions.style
  }
  const formatted = new Intl.NumberFormat(locales,opcions).format(number)
  return number < 0 ? `$ ${formatted}` : formatted
}

/**Valida si es number */
export const isNumber = (value: string) => {
  return /(?=.*?[0-9])\w+/g.test(value);
}
/**Capitaliza strings */
export const toCapitalize = (str: string) => {
  return str
    .trim()
    .toLowerCase()
    .replace(/\w\S*/g, (w) => w.replace(/^\w/, (c) => c.toUpperCase()));
}
/**Forxa los dos decimales a un numero */
export const numberToFormat = (numberToFormat: string) => {
  return Number.parseFloat(numberToFormat).toFixed(2);
} 


/**
 * Funcion Auxiliar del prepareInvoiceToCSV()
 * limpia los etiquetados html y espacios dobles
 * @param str 
 * @returns string limpio
 */
export const clearObs = (str: string): string => {
  const newStr = str.replace(REGEX.TAG_HTML, '');
  return newStr.replace(REGEX.SPACES, ' ');
}
/**
 * Funcion que limpia las entidades especiales de html
 * y saltos de lineas
 *  * '&Ntilde' => 'Ñ'
 *  * '&iacute' => 'í'
 *  * '&nbsp' => ' '
 *  * '\t', => ' '
 * @param str cadena de texto
 * @returns string limpio
 */
export const clearHtmlEntities = (str: string) => {
  return str.replace('&ntilde;', 'ñ')
            .replace('&Ntilde;', 'Ñ')
            .replace('&amp;', '&')
            .replace('&Ntilde;', 'Ñ')
            .replace('&ntilde;', 'ñ')
            .replace('&Ntilde;', 'Ñ')
            .replace('&Agrave;', 'À')
            .replace('&Aacute;', 'Á')  
            .replace('&Acirc;','Â')
            .replace('&Atilde;','Ã')   
            .replace('&Auml;','Ä')
            .replace('&Aring;','Å')
            .replace('&AElig;','Æ')
            .replace('&Ccedil;','Ç')
            .replace('&Egrave;','È')
            .replace('&Eacute;','É')
            .replace('&Ecirc;', 'Ê')
            .replace('&Euml;','Ë')
            .replace('&Igrave;', 'Ì')
            .replace('&Iacute;', 'Í')
            .replace('&Icirc;', 'Î')
            .replace('&Iuml;', 'Ï')
            .replace('&ETH;', 'Ð')
            .replace('&Ntilde;', 'Ñ')
            .replace('&Ograve;', 'Ò')
            .replace('&Oacute;', 'Ó')
            .replace('&Ocirc;', 'Ô' )
            .replace('&Otilde;', 'Õ')
            .replace('&Ouml;', 'Ö'  )
            .replace('&Oslash;', 'Ø')
            .replace('&Ugrave;' ,'Ù')
            .replace('&Uacute;', 'Ú')
            .replace('&Ucirc;', 'Û')
            .replace('&Uuml;', 'Ü')
            .replace('&Yacute;', 'Ý')
            .replace('&THORN;', 'Þ' )
            .replace('&szlig;', 'ß')
            .replace('&agrave;', 'à')
            .replace('&aacute;', 'á')
            .replace('&acirc;', 'â')
            .replace('&atilde;', 'ã')
            .replace('&auml;', 'ä')
            .replace('&aring;', 'å')
            .replace('&aelig;', 'æ')
            .replace('&ccedil;', 'ç')
            .replace('&egrave;', 'è')
            .replace('&eacute;', 'é')
            .replace('&ecirc;', 'ê')
            .replace('&euml;', 'ë')
            .replace('&igrave;', 'ì')
            .replace('&iacute;', 'í')
            .replace('&icirc;', 'î')
            .replace('&iuml;', 'ï')
            .replace('&eth;', 'ð')
            .replace('&ntilde;', 'ñ')
            .replace('&ograve;','ò')
            .replace('&oacute;','ó')
            .replace('&ocirc;','ô')
            .replace('&otilde;','õ')
            .replace('&ouml;','ö')
            .replace('&oslash;','ø')
            .replace('&ugrave;','ù')
            .replace('&uacute;','ú')
            .replace('&ucirc;','û')
            .replace('&uuml;' , 'ü')   
            .replace('&yacute;', 'ý')  
            .replace('&thorn;', 'þ')
            .replace('&yuml;', 'ÿ')
            .replace('&nbsp;', ' ')
            .replace('\t', ' ')
            .replace('\r', ' ')
            .replace('\n', ' ');
}

export const filterTransporters = (data: IAveTransp[]) => {
  const blackList = [1012, 1013 , 21, 1013, 1023, 1015, 1020];
  return data.filter(t => {
    if (!blackList.includes(Number(t.id)) ) {
      return {
        label: t.text,
        value: t.id
      } 
    }
  })
}

// /**
//  * Concatena un objeto con otro
//  */
// export const jsonConcat = (obj1: object, obj2: object): object => {
//   for (const key in obj2) {
//     obj1[key] = obj2[key];
//   }
//   return obj1;
// }
