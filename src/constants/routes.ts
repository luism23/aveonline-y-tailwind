export const ROUTES = {


  ESTADO_CUENTA: {
    PATH: '/appdev/modulos/carteraV2/',
    NAME: 'estado_cuenta'
  },
  DETALLE_COMERCIAL: {
    PATH: '/appdev/modulos/carteraV2/detalleComercial',
    NAME: 'detalleComercial'
  },
 
  RECAUDOS: {
    PATH: '/appdev/modulos/carteraV2/recuados',
    NAME: 'recuados'
  },
  REGISTRO: {
    PATH: '/appdev/modulos/carteraV2/registroNuevoCliente',
    NAME: 'registroNuevoCliente'
  },
  REGISTRO_NIT: {
    PATH: '/appdev/modulos/carteraV2/RegistroNit',
    NAME: 'RegistroNit'
  },
  DETALLE_REFERIDOS: {
    PATH: '/appdev/modulos/carteraV2/Referidos',
    NAME: 'Referidos'
  },
  REFERIDOS_COORDINADOR: {
    PATH: '/appdev/modulos/carteraV2/Comisiones',
    NAME: 'REFERIDOS_COORDINADOR'
  },
  EMBUDO_COMERCIAL: {
    PATH: '/appdev/modulos/carteraV2/Embudo',
    NAME: 'EMBUDO_COMERCIAL'
  },
  SHOPY: {
    PATH: '/appdev/modulos/carteraV2/Shopy',
    NAME: 'SHOPY'
  },
  SHOPY_CONFIRM: {
    PATH: '/appdev/modulos/carteraV2/Shopy-Confirm',
    NAME: 'SHOPY_CONFIRM'
  },
  SHOPY_EXCCED: {
    PATH: '/appdev/modulos/carteraV2/Shopy-Excced',
    NAME: 'SHOPY_EXCCED'
  },
  SHOPY_REFUSE: {
    PATH: '/appdev/modulos/carteraV2/Shopy-Refuse',
    NAME: 'SHOPY_REFUSE'
  },

  
  

  
}