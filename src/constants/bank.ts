
export const medios_pagos  = [
    {
        label: 'Consignación Bancolombia',
        value: 1
    },
    {
        label: 'Transferencia Bancolombia',
        value: 2
    }
]

export const bancos = [
    {
        label: 'Bancolombia',
        value: 1
    },
    {
        label: 'Davivienda',
        value: 2
    },
    {
        label: 'Falabela',
        value: 3
    },
    {
        label: 'Popular',
        value: 4
    },
    {
        label: 'AV villas',
        value: 5
    }
]

export const banco_documentos = [
    {
        label: 'Cédula',
        value: 1
    },
    {
        label: 'Cédula Extranjeria',
        value: 2
    },
    {
        label: 'NIT',
        value: 3
    },
    {
        label: 'Tarjeta de identidad',
        value: 4
    },
    {
        label: 'Pasaporte',
        value: 5
    }
]

export const tipos_cuentas = [
    {
        label: 'Ahorro',
        value: 38
    },
    {
        label: 'Corriente',
        value: 28
    }
]