export const tipos_ducumentos = [
    {
        label: 'Cédula',
        value: 1
    },
    {
        label: 'NIT',
        value: 2
    },
    {
        label: 'Pasaporte',
        value: 3
    },
    {
        label: 'Cédula Extranjera',
        value: 4
    }
]



export const clStandard    = 'p-2 rounded-[5px] border border-gray-400 focus:outline-none input_no_icon';
export const clErr         = 'p-2 rounded-[5px] border border-red-400  focus:outline-none input_no_icon';
export const clOK          = 'p-2 rounded-[5px] border border-green-400 font-medium focus:outline-none input_no_icon text-green-600 ';
export const clTexArea     = 'p-2 w-full h-[80px] rounded-[5px] border border-gray-400 focus:outline-none ';
export const clTexAreaErr  = 'p-2 w-full h-[80px] rounded-[5px] border border-red-400 focus:outline-none ';
export const clTexAreaOK   = 'p-2 w-full h-[80px] rounded-[5px] border border-green-400 font-medium focus:outline-none ';
