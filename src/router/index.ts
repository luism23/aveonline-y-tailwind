import { createRouter, createWebHistory, RouteRecordRaw } from "vue-router";
import { ROUTES } from "@/constants";

const routes: Array<RouteRecordRaw> = [
  
  {
    path: ROUTES.DETALLE_COMERCIAL.PATH,
    name: ROUTES.DETALLE_COMERCIAL.NAME,
    // route level code-splitting
    // this generates a separate chunk ('App_v2.detalle_comercial.[hash].js) for this route
    // which is lazy-loaded when the route is visited.
    component: () => import(/* webpackChunkName: "detalle_comercial" */ '../views/ViewCommercialDetail.vue')
  },
 
  {
    path: ROUTES.REGISTRO.PATH,
    name: ROUTES.REGISTRO.NAME,
    // route level code-splitting
    // this generates a separate chunk ('App_v2.detalle_comercial.[hash].js) for this route
    // which is lazy-loaded when the route is visited.
    component: () => import(/* webpackChunkName: "detalle_comercial" */ '../views/ViewRegistroNew.vue')
  },
  {
    path: ROUTES.REGISTRO_NIT.PATH,
    name: ROUTES.REGISTRO_NIT.NAME,
    // route level code-splitting
    // this generates a separate chunk ('App_v2.detalle_comercial.[hash].js) for this route
    // which is lazy-loaded when the route is visited.
    component: () => import(/* webpackChunkName: "detalle_comercial" */ '../views/ViewRegistroNit.vue')
  },
  {
    path: ROUTES.DETALLE_REFERIDOS.PATH,
    name: ROUTES.DETALLE_REFERIDOS.NAME,
    // route level code-splitting
    // this generates a separate chunk ('App_v2.detalle_comercial.[hash].js) for this route
    // which is lazy-loaded when the route is visited.
    component: () => import(/* webpackChunkName: "detalle_comercial" */ '../views/ViewDetalleClienteReferidos.vue')
  },
  {
    path: ROUTES.REFERIDOS_COORDINADOR.PATH,
    name: ROUTES.REFERIDOS_COORDINADOR.NAME,
    // route level code-splitting
    // this generates a separate chunk ('App_v2.detalle_comercial.[hash].js) for this route
    // which is lazy-loaded when the route is visited.
    component: () => import(/* webpackChunkName: "detalle_comercial" */ '../views/ViewReferidosCoordinadorComercial.vue')
  },
  {
    path: ROUTES.EMBUDO_COMERCIAL.PATH,
    name: ROUTES.EMBUDO_COMERCIAL.NAME,
    // route level code-splitting
    // this generates a separate chunk ('App_v2.detalle_comercial.[hash].js) for this route
    // which is lazy-loaded when the route is visited.
    component: () => import(/* webpackChunkName: "detalle_comercial" */ '../views/ViewEmbudoComercial.vue')
  },
  {
    path: ROUTES.SHOPY.PATH,
    name: ROUTES.SHOPY.NAME,
    // route level code-splitting
    // this generates a separate chunk ('App_v2.detalle_comercial.[hash].js) for this route
    // which is lazy-loaded when the route is visited.
    component: () => import(/* webpackChunkName: "detalle_comercial" */ '../views/ViewShopy.vue')
  },
  {
    path: ROUTES.SHOPY_CONFIRM.PATH,
    name: ROUTES.SHOPY_CONFIRM.NAME,
    // route level code-splitting
    // this generates a separate chunk ('App_v2.detalle_comercial.[hash].js) for this route
    // which is lazy-loaded when the route is visited.
    component: () => import(/* webpackChunkName: "detalle_comercial" */ '../views/ViewShopyCorfirmed.vue')
  },
  {
    path: ROUTES.SHOPY_EXCCED.PATH,
    name: ROUTES.SHOPY_EXCCED.NAME,
    // route level code-splitting
    // this generates a separate chunk ('App_v2.detalle_comercial.[hash].js) for this route
    // which is lazy-loaded when the route is visited.
    component: () => import(/* webpackChunkName: "detalle_comercial" */ '../views/ViewShopyOrderExceed.vue')
  },
  {
    path: ROUTES.SHOPY_REFUSE.PATH,
    name: ROUTES.SHOPY_REFUSE.NAME,
    // route level code-splitting
    // this generates a separate chunk ('App_v2.detalle_comercial.[hash].js) for this route
    // which is lazy-loaded when the route is visited.
    component: () => import(/* webpackChunkName: "detalle_comercial" */ '../views/ViewShopyRefused.vue')
  },
  
];

const router = createRouter({
  history: createWebHistory(process.env.BASE_URL),
  routes,
});

export default router;
