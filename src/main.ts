import { createApp } from 'vue';
import { createPinia } from 'pinia';  
import App from './App.vue';
import axios from 'axios';
import VueAxios from 'vue-axios';
import router from './router';
// import VTooltip from 'v-tooltip';
// import { VTooltip, VPopover, VClosePopover } from 'v-tooltip'

import { library, IconDefinition } from '@fortawesome/fontawesome-svg-core';
import { faUserSecret } from '@fortawesome/free-solid-svg-icons';
import { FontAwesomeIcon } from '@fortawesome/vue-fontawesome';
import { faFontAwesome } from '@fortawesome/free-brands-svg-icons';
import { fas } from '@fortawesome/pro-solid-svg-icons'
import { far } from '@fortawesome/pro-regular-svg-icons'
import { fal } from '@fortawesome/pro-light-svg-icons'
import { fad } from '@fortawesome/pro-duotone-svg-icons'
import { fab } from '@fortawesome/free-brands-svg-icons'

import VueTheMask from 'vue-the-mask';
import money from 'v-money';

library.add(faUserSecret as IconDefinition,
    faFontAwesome, fas,far,fal,fad, fab);

import './index.css';


createApp(App)
    .use(VueAxios, axios)
    .use(createPinia())
    .use(router)
    .use(money)
    .use(VueTheMask)
    .component('FontAwesomeIcon',FontAwesomeIcon)
    // .use(VTooltip)
    .mount('#app')
