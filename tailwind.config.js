module.exports = {
    content: [
      "./index.html",
      "./src/**/*.{vue,js,ts,jsx,tsx}",
    ],
    theme: {
        extend: {
            colors: {
              gray: {
                50: '#F8F8F8',
                100: '#F1F1F1',
                200: '#E2E2E2',
                300: '#D8D8D8',
                400: '#C2C2C2',
                450: '#A7A6A6',
                500: '#8F8F90',
                600: '#8A8A8A',
                800: '#42484E',
                900:'#A2ABB4',
                1000:'#FFFFFF',
                'btn': '#C2C2C2',
                'b-hov': '#8A8A8A',
                'b-act': '#42484E',
              },
              red: {
                50: ' #E8BFCA',
                100: '#EABCC7',
                150: '#F7DFE5',
                200: '#E5E5E5',
                300: '#DD341D',
                400: '#DF1125',
                500: '#C80935',
                600: '#B0052D',
                700: '#B21A1B',
                800: '#981433',
                900: '#8E181B',
                'btn': '#C80935',
                'b-hov': '#B0052D',
                'b-act': '#981433',
                'error': '#D81023',
              },
              amber: {
                100: '#E15B24',
                200: '#EDE437',
                300: '#E3E030',
                400: '#F7D718',
                500: '#FBAB2D',
                600: '#F9B017',
                700: '#F28C1B',
                800: '#F18403',
                900: '#ED6E1D'
              },
              emerald: {
                100: '#C7F7D7',
                200: '#BEC61A',
                300: '#1DDD5E',
                350: '#1BC755',
                400: '#17BE50',
                500: '#94A611',
                600: '#139840',
                700: '#528D17',
                800: '#107E35',
                900: '#0F642C',
                1000: '#D0D82B'
              },
              blue: {
                600: '#2F6997'
              },
              
            },
            fontFamily: {
              sansBold: 'Roboto-bold',
              sans: 'Roboto',
              serif: 'Poppins',
              serifBold: 'Poppins-bold'
            },
            borderRadius: {
              'ave-sm': '1.25rem'
            },
            boxShadow: {
              'ave-card-primary': '0px 8px 16px 0px #E5E5E5'
            },
        }
    },
    plugins: [],
  }